#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include <opengl-demo/game-engine/systems/camera-system.hpp>
#include <opengl-demo/game-engine/components/camera-component.hpp>

using namespace GLDemo::GameEngine;

void CameraSystem::update(double timeElapsed)
{
	auto entities = getEntities();
	for(auto& entity: entities){
		process(entity, timeElapsed);
	}
}

void CameraSystem::process(anax::Entity& entity, double)
{
	for(auto cursorMovement: m_CursorMovements){
		auto oldCursorPosition = std::get<0>(cursorMovement);
		auto cursorPosition    = std::get<1>(cursorMovement);

		glm::vec2 delta = (glm::vec2) glm::dvec2(
			(cursorPosition.x - oldCursorPosition.x) * MOUSE_SENSITIVITY
			, (oldCursorPosition.y - cursorPosition.y) * MOUSE_SENSITIVITY
		);

		rotate(entity, delta);
	}

	for(auto yoffset: m_yoffsets){
		zoom(entity, yoffset);
	}

	for(auto direction: m_Directions){
		auto front = entity.getComponent<CameraComponent>().getFrontVector();
		auto right = entity.getComponent<CameraComponent>().getRightVector();
		switch(direction){
			case(Direction::front):
				translate(entity, m_MoveSpeed * front);
				break;
			case(Direction::back):
				translate(entity, -m_MoveSpeed * front);
				break;
			case(Direction::left):
				translate(entity, -m_MoveSpeed * right);
				break;
			case(Direction::right):
				translate(entity, m_MoveSpeed * right);
				break;
			default:
				break;
		}
	}

	m_Directions.clear();
	m_CursorMovements.clear();
	m_yoffsets.clear();
}

void CameraSystem::processCursorMovement(const glm::dvec2& oldCursorPosition
	, const glm::dvec2& cursorPosition
	)
{
	auto positionInformation = std::make_tuple(oldCursorPosition, cursorPosition);
	m_CursorMovements.push_back(positionInformation);
}

void CameraSystem::processScroll(double yoffset)
{
	m_yoffsets.push_back(yoffset);
}

void CameraSystem::translate(anax::Entity& entity, const glm::vec3& translation)
{
	glm::vec3 tmpPos = entity.getComponent<CameraComponent>().position;
	auto translationCopy = translation;
	if(tmpPos.y + translation.y < 1.0f){
		translationCopy.y = 0.0f;
	}
	entity.getComponent<CameraComponent>().position += translationCopy;
	entity.getComponent<CameraComponent>().target += translationCopy;
}

void CameraSystem::rotate(anax::Entity& entity, float angle, const glm::vec3& axis)
{
	auto& position = entity.getComponent<CameraComponent>().position;
	auto& target = entity.getComponent<CameraComponent>().target;

	glm::vec3 tmpPos = position;
	tmpPos = glm::rotate(position, angle, axis);
	if(tmpPos.y > 1.0f){
		position = tmpPos;
		target = glm::rotate(target, angle, axis);
	}
}

void CameraSystem::rotate(anax::Entity& entity, const glm::vec2& delta)
{
	auto& camera = entity.getComponent<CameraComponent>();
	rotate(entity, glm::radians(delta.x), camera.getUpVector());

	auto pitch = glm::degrees(std::asin(camera.getFrontVector().y)) + delta.y;
	if(pitch > MIN_PITCH && pitch < MAX_PITCH){
		rotate(entity, glm::radians(delta.y), camera.getRightVector());
	}
}

void CameraSystem::zoom(anax::Entity& entity, float value)
{
	auto& camera = entity.getComponent<CameraComponent>();
	auto& position = camera.position;
	auto& target = camera.target;

	auto deltaZoom = camera.getFrontVector() * value;
	auto distanceToTarget = glm::distance(position + deltaZoom, target);

	if (distanceToTarget > MIN_ZOOM && distanceToTarget < MAX_ZOOM) {
		position += deltaZoom;
	}
}

void CameraSystem::processKeyInput(Direction direction)
{
	m_Directions.push_back(direction);
}
