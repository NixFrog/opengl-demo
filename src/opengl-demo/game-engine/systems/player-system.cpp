#include <opengl-demo/game-engine/systems/player-system.hpp>

using namespace GLDemo::GameEngine;

void PlayerSystem::update(double timeElapsed, const glm::vec3& frontVector, const glm::vec3& rightVector)
{
	auto entities = getEntities();
	for(auto& entity: entities){
		auto& velocity = entity.getComponent<VelocityComponent>();
		velocity.velocity = glm::vec3(0.0f, 0.0f, 0.0f);
		for(auto direction: m_Directions){
			switch(direction){
				case(Direction::front):
					velocity.velocity += frontVector * m_MoveSpeed;
					break;
				case(Direction::back):
					velocity.velocity -= frontVector * m_MoveSpeed;
					break;
				case(Direction::left):
					velocity.velocity -= rightVector * m_MoveSpeed;
					break;
				case(Direction::right):
					velocity.velocity += rightVector * m_MoveSpeed;
					break;
				default:
					break;
			}
		}
	}

	m_Directions.clear();
}

void PlayerSystem::processKeyInput(Direction direction)
{
	m_Directions.push_back(direction);
}
