#include <opengl-demo/game-engine/systems/movement-system.hpp>

using namespace GLDemo::GameEngine;
#include <iostream>
void MovementSystem::update(double timeElapsed)
{
	auto entities = getEntities();
	for(auto& entity: entities){
		auto& position = entity.getComponent<PositionComponent>().position;
		auto& velocity = entity.getComponent<VelocityComponent>().velocity;

		position += velocity;
	}
}
