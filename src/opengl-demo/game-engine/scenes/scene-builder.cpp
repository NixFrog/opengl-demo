#include <opengl-demo/game-engine/scenes/scene-builder.hpp>

#include <opengl-demo/game-engine/components/ambient-light-component.hpp>
#include <opengl-demo/game-engine/components/directional-light-component.hpp>
#include <opengl-demo/game-engine/components/point-light-component.hpp>
#include <opengl-demo/game-engine/components/model-component.hpp>
#include <opengl-demo/game-engine/components/position-component.hpp>
#include <opengl-demo/graphics-engine/components/skybox-component.hpp>

using namespace GLDemo::GameEngine;
using namespace GLDemo::GraphicsEngine;

Scene SceneBuilder::buildDefaultScene(anax::World& world)
{
	Scene defaultScene;
	std::vector<anax::Entity> pointLights;
	std::vector<anax::Entity> untrackedEntities;
	anax::Entity ambientLight, directionalLight, groundEntity, skyboxEntity;

	generateTrees(world, 50);

	Model groundModel("../resources/models/ground/ground.obj");
	groundModel.scale(glm::vec3(50.0, 1.0, 50.0));
	groundEntity = world.createEntity();
	groundEntity.addComponent<ModelComponent>(groundModel);
	groundEntity.addComponent<PositionComponent>(glm::vec3(0.0f, 0.0f, 0.0f));
	groundEntity.activate();

	Skybox skybox("../resources/models/skyboxes/sea/");
	skyboxEntity = world.createEntity();
	skyboxEntity.addComponent<SkyboxComponent>(skybox);
	skyboxEntity.activate();

	ambientLight = world.createEntity();
	ambientLight.addComponent<AmbientLightComponent>(
			glm::vec4(1.2f, 1.2f, 1.2f, 1.0f)
			);
	ambientLight.activate();

	directionalLight = world.createEntity();
	directionalLight.addComponent<DirectionalLightComponent>(
			glm::vec4(3.f,3.f,3.f,1.f)
			, glm::vec3(1.f, -1.f, -0.5f)
			);
	directionalLight.activate();

	for(size_t i = 0; i < 4; ++i){
		pointLights.push_back(world.createEntity());
		pointLights.at(i).addComponent<PointLightComponent>(
				glm::vec4(10.f,10.f,100.f,1.f)
				, glm::vec3(10.f, 5.f, 0.f)
				, 0.1f
				, 0.09f
				, 0.05f
				);
		pointLights.at(i).activate();

	}

	//untrackedEntities.push_back(modelEntity);
	defaultScene.setAmbientLight(ambientLight);
	defaultScene.setDirectionalLight(directionalLight);
	defaultScene.setPointLights(pointLights);
	defaultScene.setUntrackedEntities(untrackedEntities);

	return defaultScene;
}

void SceneBuilder::generateTrees(anax::World& world, size_t treeCount)
{
	anax::Entity treeEntity;

	for(size_t i = 0; i < treeCount; ++i){
		float x = XMINTREE + static_cast<float>(std::rand()) / (static_cast<float>(RAND_MAX / (XMAXTREE - XMINTREE)));
		float z = YMINTREE + static_cast<float>(std::rand()) / (static_cast<float>(RAND_MAX / (YMAXTREE - YMINTREE)));
		float angle = static_cast<float>(std::rand()) / (static_cast<float>(RAND_MAX / (360)));
		Model model("../resources/models/tree/Forest_tree.obj");
		model.rotate(angle, glm::vec3(0.0f, 1.0f, 0.0f));
		treeEntity = world.createEntity();
		treeEntity.addComponent<ModelComponent>(model);
		treeEntity.addComponent<PositionComponent>(glm::vec3(x, 0.0f, z));
		treeEntity.activate();
	}
}
