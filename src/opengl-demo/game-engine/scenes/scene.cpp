#include <opengl-demo/game-engine/scenes/scene.hpp>
#include <opengl-demo/game-engine/components/ambient-light-component.hpp>
#include <opengl-demo/game-engine/components/directional-light-component.hpp>
#include <opengl-demo/game-engine/components/point-light-component.hpp>

using namespace GLDemo::GameEngine;

void Scene::deactivate()
{
	m_AmbientLight.deactivate();
	m_DirectionalLight.deactivate();

	for(auto pointLight : m_PointLights){
		pointLight.deactivate();
	}

	for(auto entity : m_UntrackedEntities){
		entity.deactivate();
	}
}

void Scene::setAmbientLight(const anax::Entity& ambientLight)
{
	m_AmbientLight = ambientLight;
}

void Scene::setDirectionalLight(const anax::Entity& directionalLight)
{
	m_DirectionalLight = directionalLight;
}

void Scene::setPointLights(const std::vector<anax::Entity>& pointLights)
{
	m_PointLights = pointLights;
}

void Scene::setUntrackedEntities(const std::vector<anax::Entity>& untrackedEntities)
{
	m_UntrackedEntities = untrackedEntities;
}

anax::Entity Scene::getAmbientLight() const
{
	return m_AmbientLight;
}

anax::Entity Scene::getDirectionalLight() const
{
	return m_DirectionalLight;
}

std::vector<anax::Entity> Scene::getPointLights() const
{
	return m_PointLights;
}
