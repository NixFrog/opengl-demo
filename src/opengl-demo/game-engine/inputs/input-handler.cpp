#include <opengl-demo/game-engine/inputs/input-handler.hpp>
#include <opengl-demo/util/direction-utils.hpp>

using namespace GLDemo::GameEngine;

InputHandler::InputHandler() noexcept
{
	m_CameraSystem = std::make_shared<CameraSystem>();
	m_PlayerSystem = std::make_shared<PlayerSystem>();
}

std::shared_ptr<CameraSystem> InputHandler::getCameraSystem() const
{
	return m_CameraSystem;
}

std::shared_ptr<PlayerSystem> InputHandler::getPlayerSystem() const
{
	return m_PlayerSystem;
}

void InputHandler::processKey(GLFWwindow*, int key, int, int, int)
{
	switch(key){
		case(GLFW_KEY_UP):
			m_PlayerSystem->processKeyInput(Direction::front);
			m_CameraSystem->processKeyInput(Direction::front);
			break;
		case(GLFW_KEY_DOWN):
			m_PlayerSystem->processKeyInput(Direction::back);
			m_CameraSystem->processKeyInput(Direction::back);
			break;
		case(GLFW_KEY_LEFT):
			m_PlayerSystem->processKeyInput(Direction::left);
			m_CameraSystem->processKeyInput(Direction::left);
			break;
		case(GLFW_KEY_RIGHT):
			m_PlayerSystem->processKeyInput(Direction::right);
			m_CameraSystem->processKeyInput(Direction::right);
			break;
		default:
			break;
	}
}

void InputHandler::processMouseButton(GLFWwindow* window, int button, int action, int)
{
    if (button == GLFW_MOUSE_BUTTON_LEFT || button == GLFW_MOUSE_BUTTON_RIGHT) {
        if (action == GLFW_PRESS) {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            glfwGetCursorPos(window, &m_CursorPosition.x, &m_CursorPosition.y);
            m_CameraIsMoving = true;
        }
        else {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            m_CameraIsMoving = false;
        }
    }
}

void InputHandler::processCursorMovement(GLFWwindow*, double xpos, double ypos)
{
    if(m_CameraIsMoving){
        auto oldCursorPosition = m_CursorPosition;
        m_CursorPosition = glm::dvec2(xpos, ypos);
        m_CameraSystem->processCursorMovement(oldCursorPosition, m_CursorPosition);
    }
}

void InputHandler::processScroll(GLFWwindow*, double, double yoffset)
{
    m_CameraSystem->processScroll(yoffset);
}
