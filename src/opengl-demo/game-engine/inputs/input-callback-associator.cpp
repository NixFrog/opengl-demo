#include <opengl-demo/game-engine/inputs/input-callback-associator.hpp>

using namespace GLDemo::GameEngine;

InputHandler InputCallbackAssociator::m_InputHandler;

InputCallbackAssociator::InputCallbackAssociator(){}

void InputCallbackAssociator::associateCallbacks(GLFWwindow* window)
{
    glfwSetKeyCallback(window, keyCallback);
    glfwSetMouseButtonCallback(window, mouseButtonCallback);
    glfwSetCursorPosCallback(window, cursorPositionCallback);
    glfwSetScrollCallback(window, scrollCallback);
}

InputCallbackAssociator& InputCallbackAssociator::getInstance()
{
    static InputCallbackAssociator instance;
    return instance;
}

void InputCallbackAssociator::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    m_InputHandler.processKey(window, key, scancode, action, mods);
}

void InputCallbackAssociator::mouseButtonCallback(GLFWwindow* window, int button, int action, int mods)
{
    m_InputHandler.processMouseButton(window, button, action, mods);
}

void InputCallbackAssociator::cursorPositionCallback(GLFWwindow* window, double xpos, double ypos)
{
    m_InputHandler.processCursorMovement(window, xpos, ypos);
}

void InputCallbackAssociator::scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    m_InputHandler.processScroll(window, xoffset, yoffset);
}

InputHandler InputCallbackAssociator::getInputHandler()
{
    return m_InputHandler;
}
