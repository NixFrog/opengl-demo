#include <opengl-demo/game-engine/components/camera-component.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>

using namespace GLDemo::GameEngine;

glm::mat4 CameraComponent::getViewMatrix() const
{
	return glm::lookAt(position, target, getUpVector());
}

glm::mat4 CameraComponent::getProjectionMatrix() const
{
	return glm::perspectiveFov(angle, width, height, zNear, zFar);
}

glm::vec3 CameraComponent::getUpVector() const
{
	return glm::normalize(glm::cross(getRightVector(), getFrontVector()));
}

glm::vec3 CameraComponent::getRightVector() const
{
	return glm::normalize(glm::cross(getFrontVector(), glm::vec3(0.f, 1.f, 0.f)));
}

glm::vec3 CameraComponent::getFrontVector() const
{
	return glm::normalize(target - position);
}
