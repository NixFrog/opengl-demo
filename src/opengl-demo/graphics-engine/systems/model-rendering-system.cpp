#include <opengl-demo/graphics-engine/systems/model-rendering-system.hpp>

using namespace GLDemo::GraphicsEngine;

ModelRenderingSystem::ModelRenderingSystem()
{
	m_ShaderManager.addShader(GL_VERTEX_SHADER, SHADER_DIR + "vertex-shaders/models/main.vs");
	m_ShaderManager.addShader(GL_FRAGMENT_SHADER, SHADER_DIR + "fragment-shaders/models/main.fs");
	m_ShaderManager.addShader(GL_FRAGMENT_SHADER, SHADER_DIR + "fragment-shaders/models/maps.fs");
	m_ShaderManager.addShader(GL_FRAGMENT_SHADER, SHADER_DIR + "fragment-shaders/lighting/blinn-phong.fs");
	m_ShaderManager.addShader(GL_FRAGMENT_SHADER, SHADER_DIR + "fragment-shaders/lighting/lights.fs");
	m_ShaderManager.linkShaders();
	m_ShaderManager.use();
}

void ModelRenderingSystem::update()
{
	auto entities = getEntities();
	for(auto& entity: entities){
		auto position = entity.getComponent<GameEngine::PositionComponent>();
		auto& model = entity.getComponent<GameEngine::ModelComponent>();
		model.model.reposition(position.position);
	}
}

void ModelRenderingSystem::render(const GameEngine::CameraComponent& cameraComponent)
{
	auto projection = cameraComponent.getProjectionMatrix();
	auto view = cameraComponent.getViewMatrix();

	m_ShaderManager.setUniform("u_View", view);
	m_ShaderManager.setUniform("u_Projection", projection);

	auto entities = getEntities();

	for(auto entity : entities){
		auto modelComponent = entity.getComponent<GameEngine::ModelComponent>();
		auto modelMatrix = modelComponent.model.getModelMatrix();

		m_ShaderManager.setUniform("u_Model", modelMatrix);
		m_ShaderManager.setUniform("u_NormalMatrix", modelComponent.model.getNormalMatrix());

		modelComponent.model.draw(m_ShaderManager);
	}
}

void ModelRenderingSystem::render(const GameEngine::CameraComponent& cameraComponent
	, const GameEngine::AmbientLightComponent& ambientLightComponent
	, const GameEngine::DirectionalLightComponent& directionalLight
	, std::vector<GameEngine::PointLightComponent> pointLights
	, GLuint shadowMap
	, glm::mat4 lightSpaceMatrix
	)
{
	m_ShaderManager.use();
	m_ShaderManager.setUniform("u_ShadowMap", (GLuint)1);

	m_ShaderManager.setUniform("u_AmbientLight.color", ambientLightComponent.lightColor);

	m_ShaderManager.setUniform("u_DirectionalLight.color", directionalLight.lightColor);
	m_ShaderManager.setUniform("u_DirectionalLight.direction", directionalLight.direction);

	for(size_t i = 0; i < MAX_LIGHT_COUNT; ++i){
		m_ShaderManager.setUniform("u_PointLight.position", pointLights.at(i).position);
		m_ShaderManager.setUniform("u_PointLight.color", pointLights.at(i).lightColor);
		m_ShaderManager.setUniform("u_PointLight.position", pointLights.at(i).position);
		m_ShaderManager.setUniform("u_PointLight.constant", pointLights.at(i).constant);
		m_ShaderManager.setUniform("u_PointLight.linear", pointLights.at(i).linear);
		m_ShaderManager.setUniform("u_PointLight.quadratic", pointLights.at(i).quadratic);
	}

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, shadowMap);
	m_ShaderManager.setUniform("u_LightSpaceMatrix", lightSpaceMatrix);

	render(cameraComponent);
}
