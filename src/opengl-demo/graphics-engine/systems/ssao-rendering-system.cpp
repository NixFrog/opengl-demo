#include <opengl-demo/graphics-engine/systems/ssao-rendering-system.hpp>
#include <opengl-demo/util/window-utils.hpp>

using namespace GLDemo::GraphicsEngine;

SSAORenderingSystem::SSAORenderingSystem()
{
	m_GeometryShaderManager.addShader(GL_VERTEX_SHADER, SHADER_DIR + "vertex-shaders/lights/ssao/geometry.vs");
	m_GeometryShaderManager.addShader(GL_FRAGMENT_SHADER, SHADER_DIR + "fragment-shaders/lighting/ssao/geometry.fs");
	m_GeometryShaderManager.linkShaders();

	m_SSAOShaderManager.addShader(GL_VERTEX_SHADER, SHADER_DIR + "vertex-shaders/lights/ssao/ssao.vs");
	m_SSAOShaderManager.addShader(GL_FRAGMENT_SHADER, SHADER_DIR + "fragment-shaders/lighting/ssao/ssao.fs");
	m_SSAOShaderManager.linkShaders();
	m_SSAOShaderManager.use();
	m_SSAOShaderManager.setUniform("gPositionDepth", (GLuint)0);
	m_SSAOShaderManager.setUniform("gNormal", (GLuint)1);
	m_SSAOShaderManager.setUniform("texNoise", (GLuint)2);

	createFrameBuffer();
	std::default_random_engine generator;
	initiateKernel(generator);
	generateNoise(generator);
	generateNoiseTexture();
	generateQuadVAO();
}

void SSAORenderingSystem::render(const GameEngine::CameraComponent& cameraComponent)
{
	const GLuint NR_LIGHTS = 2;
	std::vector<glm::vec3> lightPositions;
	std::vector<glm::vec3> lightColors;
	srand(13);
	for (GLuint i = 0; i < NR_LIGHTS; i++){
		// Calculate slightly random offsets
		GLfloat xPos = ((rand() % 100) / 100.0) * 6.0 - 3.0;
		GLfloat yPos = ((rand() % 100) / 100.0) * 6.0 - 4.0;
		GLfloat zPos = ((rand() % 100) / 100.0) * 6.0 - 3.0;
		lightPositions.push_back(glm::vec3(xPos, yPos, zPos));
		// Also calculate random color
		GLfloat rColor = ((rand() % 100) / 200.0f) + 0.5; // Between 0.5 and 1.0
		GLfloat gColor = ((rand() % 100) / 200.0f) + 0.5; // Between 0.5 and 1.0
		GLfloat bColor = ((rand() % 100) / 200.0f) + 0.5; // Between 0.5 and 1.0
		lightColors.push_back(glm::vec3(rColor, gColor, bColor));
	}

	auto entities = getEntities();
	glm::mat4 view = cameraComponent.getViewMatrix();
	auto projection = cameraComponent.getProjectionMatrix();

	glBindFramebuffer(GL_FRAMEBUFFER, m_gBuffer.getBuffer());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		m_GeometryShaderManager.use();
		m_GeometryShaderManager.setUniform("u_Projection", projection);
		m_GeometryShaderManager.setUniform("u_View", view);
		m_GeometryShaderManager.setUniform("u_NearPlane", cameraComponent.zNear);
		m_GeometryShaderManager.setUniform("u_FarPlane", cameraComponent.zFar);

		for(auto entity : entities){
			auto modelComponent = entity.getComponent<GameEngine::ModelComponent>();
			auto modelMatrix = modelComponent.model.getModelMatrix();
			m_GeometryShaderManager.setUniform("u_Model", modelMatrix);
			m_GeometryShaderManager.setUniform("u_NormalMatrix", modelComponent.model.getNormalMatrix());
			modelComponent.model.draw(m_GeometryShaderManager);
		}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glBindFramebuffer(GL_FRAMEBUFFER, m_ssaoFBO);
		glClear(GL_COLOR_BUFFER_BIT);
		m_SSAOShaderManager.use();
		m_SSAOShaderManager.setUniform("gPositionDepth", (GLuint)0);
		m_SSAOShaderManager.setUniform("gNormal", (GLuint)1);
		m_SSAOShaderManager.setUniform("texNoise", (GLuint)2);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_gBuffer.getPositionDepthTexture());
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, m_gBuffer.getNormalTexture());
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, m_NoiseTexture);

		for (GLuint i = 0; i < 64; ++i){
			m_SSAOShaderManager.setUniform(
					("samples[" + std::to_string(i) + "]").c_str()
					, &m_ssaoKernel[i][0]
			);
		}
		m_SSAOShaderManager.setUniform("u_Projection", glm::value_ptr(projection));
		renderQuad();
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	ShaderManager lightPgm;
	lightPgm.addShader(GL_VERTEX_SHADER, SHADER_DIR + "vertex-shaders/lights/deferred/test.vs");
	lightPgm.addShader(GL_FRAGMENT_SHADER, SHADER_DIR + "fragment-shaders/lighting/deferred/test.fs");
	lightPgm.linkShaders();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	lightPgm.use();
	lightPgm.setUniform("gPosition", (GLuint)0);
	lightPgm.setUniform("gNormal", (GLuint)1);
	lightPgm.setUniform("gAlbedoSpec", (GLuint)2);
	lightPgm.setUniform("ssao", (GLuint)3);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_gBuffer.getPositionDepthTexture());
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_gBuffer.getNormalTexture());
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_gBuffer.getAlbedoTexture());
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_ssaoColorBuffer);
	// Also send light relevant uniforms
	for (GLuint i = 0; i < lightPositions.size(); i++)
	{
		lightPgm.setUniform(("lights[" + std::to_string(i) + "].Position").c_str(), &lightPositions[i][0]);
		lightPgm.setUniform(("lights[" + std::to_string(i) + "].Color").c_str(), &lightColors[i][0]);
		// Update attenuation parameters and calculate radius
		const GLfloat linear = 0.7;
		const GLfloat quadratic = 1.8;
		lightPgm.setUniform(("lights[" + std::to_string(i) + "].Linear").c_str(), linear);
		lightPgm.setUniform(("lights[" + std::to_string(i) + "].Quadratic").c_str(), quadratic);
	}
	lightPgm.setUniform("viewPos", cameraComponent.position);
	renderQuad();
}

void SSAORenderingSystem::createFrameBuffer()
{
	glGenFramebuffers(1, &m_ssaoFBO);//  glGenFramebuffers(1, &ssaoBlurFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, m_ssaoFBO);

	glGenTextures(1, &m_ssaoColorBuffer);
	glBindTexture(GL_TEXTURE_2D, m_ssaoColorBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_RGB, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_ssaoColorBuffer, 0);
	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE){
		std::cerr << "Error initializing SSAO: Framebuffer not complete." << std::endl;
	}
	//
	//glBindFramebuffer(GL_FRAMEBUFFER, ssaoBlurFBO);
	//glGenTextures(1, &ssaoColorBufferBlur);
	//glBindTexture(GL_TEXTURE_2D, ssaoColorBufferBlur);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, SCR_WIDTH, SCR_HEIGHT, 0, GL_RGB, GL_FLOAT, NULL);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, ssaoColorBufferBlur, 0);
	//if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	//std::cout << "SSAO Blur Framebuffer not complete!" << std::endl;
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void SSAORenderingSystem::initiateKernel(std::default_random_engine& generator)
{
	std::uniform_real_distribution<GLfloat> randomFloats(0.0, 1.0);

	for (GLuint i = 0; i < 64; ++i){
		glm::vec3 sample(
			randomFloats(generator) * 2.0 - 1.0
			, randomFloats(generator) * 2.0 - 1.0
			, randomFloats(generator)
		);
		sample = glm::normalize(sample);
		sample *= randomFloats(generator);
		GLfloat scale = GLfloat(i) / 64.0;
		scale = lerp(0.1f, 1.0f, scale * scale);
		sample *= scale;
		m_ssaoKernel.push_back(sample);
	}
}

GLfloat SSAORenderingSystem::lerp(GLfloat a, GLfloat b, GLfloat f)
{
	return a + f * (b - a);
}

void SSAORenderingSystem::generateNoise(std::default_random_engine& generator)
{
	std::uniform_real_distribution<GLfloat> randomFloats(0.0, 1.0);
	for (GLuint i = 0; i < 16; i++){
		glm::vec3 noise(
			randomFloats(generator) * 2.0 - 1.0
			, randomFloats(generator) * 2.0 - 1.0
			, 0.0f
		);
		m_ssaoNoise.push_back(noise);
	}
}

void SSAORenderingSystem::generateNoiseTexture()
{
	glGenTextures(1, &m_NoiseTexture);
	glBindTexture(GL_TEXTURE_2D, m_NoiseTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, 4, 4, 0, GL_RGB, GL_FLOAT, &m_ssaoNoise[0]); //rgb16f for color values >1.0
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void SSAORenderingSystem::generateQuadVAO()
{
	GLuint quadVBO;
	GLfloat quadVertices[] = {
		// Positions        	// Texture Coords
		-1.0f,  1.0f, 0.0f,		0.0f, 1.0f,
		-1.0f, -1.0f, 0.0f, 	0.0f, 0.0f,
		1.0f,   1.0f, 0.0f,		1.0f, 1.0f,
		1.0f,  -1.0f, 0.0f,		1.0f, 0.0f,
	};

	glGenVertexArrays(1, &m_QuadVAO);
	glGenBuffers(1, &quadVBO);

	glBindVertexArray(m_QuadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
	glBindVertexArray(0);
}

void SSAORenderingSystem::renderQuad()
{
	glBindVertexArray(m_QuadVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
}
