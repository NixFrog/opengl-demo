#include <opengl-demo/graphics-engine/systems/directional-shadow-mapping-system.hpp>
#include <opengl-demo/util/window-utils.hpp>

using namespace GLDemo::GraphicsEngine;

DirectionalShadowMappingSystem::DirectionalShadowMappingSystem()
{
	glGenFramebuffers(1, &m_DepthMapFBO);
	m_ShadowMap = newDepthMap();
	attachDepthMap(m_ShadowMap);

	m_ShaderManager.addShader(GL_VERTEX_SHADER, SHADER_DIR + "vertex-shaders/lights/light-space-transform.vs");
	m_ShaderManager.addShader(GL_FRAGMENT_SHADER, SHADER_DIR + "fragment-shaders/empty.fs");
	m_ShaderManager.linkShaders();
}

GLuint DirectionalShadowMappingSystem::newDepthMap()
{
	GLuint depthMap;

	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT,
				SHADOW_WIDTH, SHADOW_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL
				);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	GLfloat borderColor[] = { 1.0, 1.0, 1.0, 1.0 }; // fix over-sampling
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

	return depthMap;
}

void DirectionalShadowMappingSystem::attachDepthMap(const GLuint& depthMap)
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_DepthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DirectionalShadowMappingSystem::render(const GameEngine::DirectionalLightComponent& directionalLight)
{
	m_ShaderManager.linkShaders();
	m_ShaderManager.use();

	glm::mat4 lightView = glm::lookAt(glm::vec3(-10.0f, 30.0f, 10.0f),
			glm::vec3( 0.0f, 0.0f,  0.0f),
			glm::vec3( 0.0f, 1.0f,  0.0f));
	m_LightSpaceMatrix = LIGHT_PROJECTION * lightView;
	m_ShaderManager.setUniform("u_LightSpaceMatrix", m_LightSpaceMatrix);

	glCullFace(GL_FRONT);

	glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
	glBindFramebuffer(GL_FRAMEBUFFER, m_DepthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	auto entities = getEntities();
	for(auto entity: entities){
		auto modelComponent = entity.getComponent<GameEngine::ModelComponent>();
		auto modelMatrix = modelComponent.model.getModelMatrix();

		m_ShaderManager.setUniform("u_Model", modelMatrix);
		modelComponent.model.draw(m_ShaderManager, false);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glCullFace(GL_BACK);
}

GLuint DirectionalShadowMappingSystem::getShadowMap() const
{
	return m_ShadowMap;
}

glm::mat4 DirectionalShadowMappingSystem::getLightSpaceMatrix() const
{
	return m_LightSpaceMatrix;
}
