#include <opengl-demo/graphics-engine/systems/skybox-rendering-system.hpp>

using namespace GLDemo::GraphicsEngine;

SkyboxRenderingSystem::SkyboxRenderingSystem()
: m_Model("../resources/models/skyboxes/skybox.obj")
{
	m_ShaderManager.addShader(GL_VERTEX_SHADER, SHADER_DIR + "vertex-shaders/models/skybox.vs");
	m_ShaderManager.addShader(GL_FRAGMENT_SHADER, SHADER_DIR + "fragment-shaders/models/skybox.fs");
	m_ShaderManager.linkShaders();
	m_ShaderManager.use();
}

void SkyboxRenderingSystem::render(const GameEngine::CameraComponent& cameraComponent)
{
	auto projection = cameraComponent.getProjectionMatrix();
	auto view = glm::mat4(glm::mat3(cameraComponent.getViewMatrix()));

	glDepthFunc(GL_LEQUAL);
	m_ShaderManager.use();
	m_ShaderManager.setUniform("u_View", view);
	m_ShaderManager.setUniform("u_Projection", projection);

	glActiveTexture(GL_TEXTURE0);

	auto entities = getEntities();

	for(auto entity: entities){
		auto skyboxComponent = entity.getComponent<SkyboxComponent>();

		glBindTexture(GL_TEXTURE_CUBE_MAP, skyboxComponent.skybox.getTextureID());
		m_Model.draw(m_ShaderManager, false);
	}

	glDepthFunc(GL_LESS);
	glBindTexture(GL_TEXTURE_2D, 0);
}
