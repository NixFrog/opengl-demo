#version 430 core

struct Material {
    sampler2D diffuseMap;
    sampler2D normalMap;

    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
};

in vec3 fs_ModelPosition;

uniform vec3 cameraPosition;
uniform Material material;

vec3 getNormal();
vec4 diffuseMapColor();

vec4 ambientColor(vec4 lightColor) {
    return lightColor * material.ambient * diffuseMapColor();
}

vec4 diffuseColor(vec4 lightColor, vec3 lightDirection) {
    float diffuseCoeff = max(dot(getNormal(), lightDirection), 0.0);
    return lightColor * material.diffuse * diffuseCoeff * diffuseMapColor();
}

vec4 specularColor(vec4 lightColor, vec3 lightDirection) {
    vec3 viewDirection = normalize(cameraPosition - fs_ModelPosition);
    vec3 reflectedDirection = reflect(-lightDirection, getNormal());

    float specularCoeff = pow(
        max(dot(viewDirection, reflectedDirection), 0.0),
        material.shininess
    );

    return lightColor * material.specular * specularCoeff;
}
