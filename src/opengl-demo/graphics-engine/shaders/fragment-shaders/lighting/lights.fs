#version 430 core

struct AmbientLight{
	vec4 color;
};

struct DirectionalLight{
	vec3 direction;
	vec4 color;
};

struct PointLight{
	vec3 position;
	vec4 color;

	float constant;
	float linear;
	float quadratic;
};

in vec3 fs_ModelPosition;
in vec4 fs_LightspacePosition;

uniform sampler2D u_ShadowMap;

uniform AmbientLight u_AmbientLight;
uniform DirectionalLight u_DirectionalLight;
uniform PointLight u_PointLight;

vec4 ambientColor();
vec4 diffuseColor(vec3 lightDirection);
vec4 specularColor(vec3 lightDirection);
vec3 getNormal();

vec4 computeAmbientLight(AmbientLight light) {
	return light.color * ambientColor();
}

float computeDirectionalShadow(vec4 lightSpacePosition, vec3 lightDirection)
{
	float shadow = 0.0;

	if(lightSpacePosition.z > 1.0){
		return 0.0;
	}

	vec3 projCoords = lightSpacePosition.xyz / lightSpacePosition.w;
	projCoords = projCoords * 0.5 + 0.5;
	float closestDepth = texture(u_ShadowMap, projCoords.xy).r;
	float currentDepth = projCoords.z;
	float bias = max(0.05 * (1.0 - dot(getNormal(), lightDirection)), 0.005);

	//PCF to smooth shadows
	vec2 texelSize = 1.0 / textureSize(u_ShadowMap, 0);
	for(int x = -1; x <= 1; ++x)
	{
		for(int y = -1; y <= 1; ++y)
		{
			float pcfDepth = texture(u_ShadowMap, projCoords.xy + vec2(x, y) * texelSize).r;
			shadow += currentDepth - bias > pcfDepth ? 1.0 : 0.0;
		}
	}
	shadow /= 9.0;

	return shadow;
}

vec4 computeDirectionalLight(DirectionalLight light) {
	vec3 lightDirection = normalize(-light.direction);

	return light.color *
		(1.0 - computeDirectionalShadow(fs_LightspacePosition, lightDirection))
		* (diffuseColor(lightDirection) + specularColor(lightDirection))
		;
}

vec4 computePointLight(PointLight light) {
	vec3 lightVec = light.position - fs_ModelPosition;
	vec3 lightDirection = normalize(lightVec);

	float distance = length(lightVec);
	float attenuation = 1.0f / (
		light.constant
		+ light.linear * distance
		+ light.quadratic * (distance * distance)
		);

	return light.color *
		(attenuation * (diffuseColor(lightDirection)
		+ specularColor(lightDirection)
		));
}

vec4 computeColor() {
	return computeAmbientLight(u_AmbientLight)
		+ computeDirectionalLight(u_DirectionalLight)
		+ computePointLight(u_PointLight);
}
