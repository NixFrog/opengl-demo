#version 430 core

struct Material {
	sampler2D diffuseMap;
	sampler2D normalMap;

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	float shininess;
};

in vec3 fs_ModelPosition;

uniform vec3 cameraPosition;
uniform Material material;

vec3 getNormal();
vec4 diffuseMapColor();

vec4 ambientColor() {
	return material.ambient * diffuseMapColor();
}

vec4 diffuseColor(vec3 lightDirection) {
	float diffuseCoeff = max(dot(getNormal(), lightDirection), 0.0);
	return material.diffuse * diffuseCoeff * diffuseMapColor();
}

vec4 specularColor(vec3 lightDirection) {
	vec3 normal = getNormal();
	vec3 viewDirection = normalize(cameraPosition - fs_ModelPosition);
	vec3 halfwayDirection = normalize(lightDirection + viewDirection);

	float specularCoeff = pow(
		max(dot(normal, halfwayDirection), 0.0),
		material.shininess
	);

	return material.specular * specularCoeff;
}
