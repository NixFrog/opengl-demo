#version 430 core
in vec2 fs_TextureCoordinates;

out float fs_FragmentColor;

uniform sampler2D gPositionDepth;
uniform sampler2D gNormal;
uniform sampler2D texNoise;

uniform vec3 samples[64];

// parameters (you'd probably want to use them as uniforms to more easily tweak the effect)
int kernelSize = 64;
float radius = 1.0;

// tile noise texture over screen based on screen dimensions divided by noise size
const vec2 noiseScale = vec2(800.0f/4.0f, 600.0f/4.0f);

uniform mat4 u_Projection;

void main()
{
	// Get input for SSAO algorithm
	vec3 fragPos = texture(gPositionDepth, fs_TextureCoordinates).xyz;
	vec3 normal = texture(gNormal, fs_TextureCoordinates).rgb;
	vec3 randomVec = texture(texNoise, fs_TextureCoordinates * noiseScale).xyz;
	// Create TBN change-of-basis matrix: from tangent-space to view-space
	vec3 tangent = normalize(randomVec - normal * dot(randomVec, normal));
	vec3 bitangent = cross(normal, tangent);
	mat3 TBN = mat3(tangent, bitangent, normal);

	float occlusion = 0.0;
	for(int i = 0; i < kernelSize; ++i){
		// get sample position
		vec3 samplePos = TBN * samples[i]; // From tangent to view-space
		samplePos = fragPos + samplePos * radius;

		// project sample position (to sample texture) (to get position on screen/texture)
		vec4 offset = vec4(samplePos, 1.0);
		offset = u_Projection * offset; // from view to clip-space
		offset.xyz /= offset.w; // perspective divide
		offset.xyz = offset.xyz * 0.5 + 0.5; // transform to range 0.0 - 1.0

		// get sample depth
		float sampleDepth = -texture(gPositionDepth, offset.xy).w; // Get depth value of kernel sample

		// range check & accumulate
		float rangeCheck = smoothstep(0.0, 1.0, radius / abs(fragPos.z - sampleDepth ));
		occlusion += (sampleDepth >= samplePos.z ? 1.0 : 0.0) * rangeCheck;
	}
	occlusion = 1.0 - (occlusion / kernelSize);

	fs_FragmentColor = occlusion;
}

