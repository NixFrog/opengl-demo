#version 430 core

struct Material {
    sampler2D diffuseMap;
    sampler2D normalMap;

    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
};

layout (location = 0) out vec4 gPositionDepth;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gAlbedoSpec;

in vec2 fs_TextureCoordinates;
in vec3 fs_FragmentPosition;
in vec3 fs_Normal;

uniform float u_NearPlane;
uniform float u_FarPlane;
uniform Material material;
out vec4 color;

float LinearizeDepth(float depth)
{
	float z = depth * 2.0 - 1.0; // Back to NDC
	return (2.0 * u_NearPlane * u_FarPlane) / (u_FarPlane + u_NearPlane - z * (u_FarPlane - u_NearPlane));
}

void main()
{
	gPositionDepth.xyz = fs_FragmentPosition;
	gPositionDepth.a = LinearizeDepth(gl_FragCoord.z);
	gNormal = normalize(fs_Normal);
	gAlbedoSpec = texture(material.diffuseMap, fs_TextureCoordinates);
}
