#version 430 core

in vec2 fs_TextureCoordinates;

out vec4 color;

uniform sampler2D u_DepthMap;

void main()
{
    float depthValue = texture(u_DepthMap, fs_TextureCoordinates).r;
    color = vec4(vec3(depthValue), 1.0);
}
