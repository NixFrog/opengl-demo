#version 430 core

out vec4 color;
/*in vec2 fs_TextureCoordinates;*/
vec4 computeColor();
/*uniform sampler2D shadowMap;*/
void main()
{
	color = computeColor();
	/*float depthValue = (shadowMap, fs_TextureCoordinates).r;*/
    /*color = vec4(vec3(depthValue), 1.0);*/
}
