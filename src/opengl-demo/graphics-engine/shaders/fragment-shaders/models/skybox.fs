#version 430 core

in vec3 fs_TextureCoordinates;

out vec4 color;

uniform samplerCube u_SkyboxSampler;

void main()
{
	color = texture(u_SkyboxSampler, fs_TextureCoordinates);
}
