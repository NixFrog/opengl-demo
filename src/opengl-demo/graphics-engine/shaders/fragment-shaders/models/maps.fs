#version 430 core

struct Material {
    sampler2D diffuseMap;
    sampler2D normalMap;

    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    float shininess;
};

in vec3 fs_Normal;
in vec2 fs_TextureCoordinates;

uniform Material material;

vec3 getNormal() {
    return normalize(fs_Normal);
}

vec4 diffuseMapColor() {
    vec4 color = texture(material.diffuseMap, fs_TextureCoordinates);
    if (color.a < 0.1) { discard; }

    return color;
}
