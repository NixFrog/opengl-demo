#include <opengl-demo/graphics-engine/shaders/shader-manager.hpp>

using namespace GLDemo::GraphicsEngine;

ShaderManager::ShaderManager()
: m_ShaderProgram(0)
{}

void ShaderManager::addShader(const GLenum& type, std::string shaderFilename)
{
	auto shaderAsString = readShader(shaderFilename);
	auto shaderCode = shaderAsString.c_str();
	auto shader = glCreateShader(type);

	if(shader == 0) {
		std::cerr<< "Error creating shader \""<< shaderFilename<< "\" of type: "<< type<< std::endl;
		exit(EXIT_FAILURE);
	}

	GLchar const* files[] = {shaderCode};
	GLint lengthSum[1] = {(GLint)shaderAsString.size()};

	glShaderSource(shader, 1, files, lengthSum);
	glCompileShader(shader);

	GLint compilationStatus;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compilationStatus);

	if(compilationStatus != GL_TRUE){
		manageShaderCompilationError(shader, shaderFilename);
	}

	m_Shaders.push_back(shader);
}

void ShaderManager::linkShaders()
{
	m_ShaderProgram = glCreateProgram();

	if(m_ShaderProgram == 0) {
		std::cerr<<"Error creating shader program"<<std::endl;
		exit(EXIT_FAILURE);
	}

	for(GLuint shader : m_Shaders) {
		glAttachShader(m_ShaderProgram, shader);
	}
	glLinkProgram(m_ShaderProgram);

	GLint linkingStatus;
	glGetProgramiv(m_ShaderProgram, GL_LINK_STATUS, &linkingStatus);
	if (linkingStatus != GL_TRUE) {
		manageLinkingError();
		exit(EXIT_FAILURE);
	}
}

void ShaderManager::use() const
{
	assert(m_ShaderProgram != 0);
	glUseProgram(m_ShaderProgram);
}

std::string ShaderManager::readShader(std::string shaderFilename) const
{
	std::ifstream ifs(shaderFilename, std::ifstream::in);

	if (!ifs) {
		std::cerr << "failed to open file " << shaderFilename << std::endl;
		exit(EXIT_FAILURE);
	}

	std::stringstream ss;
	ss << ifs.rdbuf();
	ifs.close();

	return ss.str();
}

void ShaderManager::manageShaderCompilationError(const GLuint& shader, std::string shaderFilename) const
{
	std::cerr << "Failed to compile shader from "<< shaderFilename << std::endl;
	GLint length;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
	auto logBuffer = new GLchar[length + 1];

	glGetShaderInfoLog(shader, length, &length, logBuffer);
	std::cerr << logBuffer << std::endl;

	delete[] logBuffer;
	exit(EXIT_FAILURE);
}

void ShaderManager::manageLinkingError() const
{
	std::cerr << "Error linking shaders:" << std::endl;
	GLint length;
	glGetProgramiv(m_ShaderProgram, GL_INFO_LOG_LENGTH, &length);
	auto logBuffer = new GLchar[length + 1];

	glGetProgramInfoLog(m_ShaderProgram, length, &length, logBuffer);
	std::cout << logBuffer << std::endl;

	delete[] logBuffer;
	exit(EXIT_FAILURE);
}

GLuint ShaderManager::getUniformLocation(std::string uniformName)
{
	auto it = m_UniformNameToIdMap.find(uniformName);

	if (it != m_UniformNameToIdMap.end()) {
		return it->second;
	}
	else {
		GLuint location = glGetUniformLocation(m_ShaderProgram, uniformName.c_str());
		if(location == 0xffffffff){
			std::cerr << "WARNING: could not get the location of uniform \""
					<< uniformName << "\"" << std::endl;
		}
		m_UniformNameToIdMap.insert({uniformName, location});
		return location;
	}
}

void ShaderManager::setUniform(std::string uniformName, const bool& value)
{
	auto name = getUniformLocation(uniformName);
	glUniform1i(name, value);
}

void ShaderManager::setUniform(std::string uniformName, const GLuint& value)
{
	auto name = getUniformLocation(uniformName);
	glUniform1i(name, value);
}

void ShaderManager::setUniform(std::string uniformName, const GLfloat& value)
{
	auto name = getUniformLocation(uniformName);
	glUniform1f(name, value);
}

void ShaderManager::setUniform(std::string uniformName, const glm::vec3& value)
{
	auto name = getUniformLocation(uniformName);
	auto value_ptr = glm::value_ptr(value);
	glUniform3fv(name, 1, value_ptr);
}

void ShaderManager::setUniform(std::string uniformName, const glm::vec4& value)
{
	auto name = getUniformLocation(uniformName);
	auto value_ptr = glm::value_ptr(value);
	glUniform4fv(name, 1, value_ptr);
}

void ShaderManager::setUniform(std::string uniformName, const glm::mat3& value)
{
	auto name = getUniformLocation(uniformName);
	auto value_ptr = glm::value_ptr(value);
	glUniformMatrix3fv(name, 1, GL_FALSE, value_ptr);
}

void ShaderManager::setUniform(std::string uniformName, const glm::mat4& value)
{
	auto name = getUniformLocation(uniformName);
	auto value_ptr = glm::value_ptr(value);
	glUniformMatrix4fv(name, 1, GL_FALSE, value_ptr);
}
