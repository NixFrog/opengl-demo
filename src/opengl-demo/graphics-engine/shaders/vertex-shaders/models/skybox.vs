#version 430 core

layout (location = 0) in vec3 vs_Position;

out vec3 fs_TextureCoordinates;

uniform mat4 u_View;
uniform mat4 u_Projection;

void main()
{
	gl_Position = (u_Projection * u_View * vec4(vs_Position, 1.0)).xyww;
	fs_TextureCoordinates = vs_Position;
}
