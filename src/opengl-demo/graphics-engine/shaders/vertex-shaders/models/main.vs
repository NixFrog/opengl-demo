#version 430 core

layout (location = 0) in vec3 vs_Position;
layout (location = 1) in vec3 vs_Normal;
layout (location = 2) in vec2 vs_TextureCoordinates;

out vec3 fs_ModelPosition;
out vec3 fs_Normal;
out vec2 fs_TextureCoordinates;
out vec4 fs_LightspacePosition;

uniform mat4 u_Model;
uniform mat4 u_View;
uniform mat4 u_Projection;
uniform mat3 u_NormalMatrix;
uniform mat4 u_LightSpaceMatrix;

void main()
{
    vec4 modelPosition = u_Model * vec4(vs_Position, 1.0f);
    fs_ModelPosition = vec3(modelPosition);
    gl_Position = u_Projection * u_View * modelPosition;
    fs_Normal = u_NormalMatrix * vs_Normal;
    fs_TextureCoordinates = vs_TextureCoordinates;
	fs_LightspacePosition = u_LightSpaceMatrix * vec4(fs_ModelPosition, 1.0);
}
