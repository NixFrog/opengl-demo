#version 430 core

layout (location = 0) in vec3 vs_Position;
layout (location = 1) in vec3 vs_Normal;
layout (location = 2) in vec2 vs_TextureCoordinates;

out vec3 fs_FragmentPosition;
out vec2 fs_TextureCoordinates;
out vec3 fs_Normal;

uniform mat4 u_Model;
uniform mat4 u_View;
uniform mat4 u_Projection;
uniform mat3 u_NormalMatrix;

void main()
{
	/*vec4 viewPos = u_View * u_Model * vec4(vs_Position, 1.0f);*/
	vec4 viewPos = u_Model * vec4(vs_Position, 1.0f);
	fs_FragmentPosition = viewPos.xyz;
	gl_Position = u_Projection * u_View * viewPos;
	/*gl_Position = u_Projection * viewPos;*/
	fs_TextureCoordinates = vs_TextureCoordinates;

	fs_Normal = u_NormalMatrix * vs_Normal;
}
