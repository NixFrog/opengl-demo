#version 430 core

layout (location = 0) in vec3 vs_Position;
layout (location = 1) in vec2 vs_TextureCoordinates;

out vec2 fs_TextureCoordinates;

void main()
{
	gl_Position = vec4(vs_Position, 1.0f);
	fs_TextureCoordinates = vs_TextureCoordinates;
}
