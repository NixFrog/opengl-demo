#include <opengl-demo/graphics-engine/models/components/vertex.hpp>

using namespace GLDemo::GraphicsEngine;

Vertex::Vertex
    ( const aiVector3D& position
    , const aiVector3D& normal
    , const aiVector3D& textureCoordinates
    , const aiVector3D& tangent
    , const aiVector3D& bitangent
    )
{
    m_Position           = glm::vec3(position.x, position.y, position.z);
    m_Normal             = glm::vec3(normal.x, normal.y, normal.z);
    // FIXME do not set if not used
    m_TextureCoordinates = glm::vec2(textureCoordinates.x, textureCoordinates.y);
    m_Tangent            = glm::vec3(tangent.x, tangent.y, tangent.z);
    m_Bitangent          = glm::vec3(bitangent.x, bitangent.y, bitangent.z);
}

void Vertex::positionAttribPointer()
{
    auto offset = (GLvoid*) offsetof(Vertex, m_Position);
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), offset);
    glEnableVertexAttribArray(0);
}

void Vertex::normalAttribPointer()
{
    auto offset = (GLvoid*) offsetof(Vertex, m_Normal);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), offset);
    glEnableVertexAttribArray(1);
}

void Vertex::textureCoordAttribPointer()
{
    auto offset = (GLvoid*) offsetof(Vertex, m_TextureCoordinates);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), offset);
    glEnableVertexAttribArray(2);
}

void Vertex::tangentAttribPointer()
{
    auto offset = (GLvoid*) offsetof(Vertex, m_Tangent);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), offset);
    glEnableVertexAttribArray(3);
}

void Vertex::bitangentAttribPointer()
{
    auto offset = (GLvoid*) offsetof(Vertex, m_Bitangent);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), offset);
    glEnableVertexAttribArray(4);
}
