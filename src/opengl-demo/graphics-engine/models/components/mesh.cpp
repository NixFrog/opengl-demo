#include <opengl-demo/graphics-engine/models/components/mesh.hpp>

using namespace GLDemo::GraphicsEngine;

Mesh::Mesh(const aiMesh& mesh, const aiMaterial& material, std::string materialDir)
: m_Material(material, materialDir)
{
    for(uint i = 0; i < mesh.mNumVertices; ++i){
        Vertex v( mesh.mVertices[i]
                , mesh.mNormals[i]
                , mesh.mTextureCoords[0][i]
                , mesh.mTangents[i]
                , mesh.mBitangents[i]
                );
        m_Vertices.push_back(v);
    }

    for(uint i = 0; i < mesh.mNumFaces; ++i){
        auto face = mesh.mFaces[i];

        for(uint j = 0; j < face.mNumIndices; ++j){
            m_Indices.push_back(face.mIndices[j]);
        }
    }

    generateBuffers();
}

void Mesh::draw(ShaderManager& shaderManager, bool isTextured)
{
	if(isTextured){
	    m_Material.update(shaderManager);

    	m_Material.bindTextures();
	}

    glBindVertexArray(m_vao);
    glDrawElements(GL_TRIANGLES, m_Indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

	if(isTextured){
	    m_Material.unbindTextures();
	}
}

void Mesh::generateBuffers()
{
    glGenVertexArrays(1, &m_vao);
    glGenBuffers(1, &m_vbo);
    glGenBuffers(1, &m_ebo);

    glBindVertexArray(m_vao);

    glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
    glBufferData(GL_ARRAY_BUFFER, m_Vertices.size() * sizeof(Vertex), &m_Vertices.front(), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_Indices.size() * sizeof(GLuint), &m_Indices.front(), GL_STATIC_DRAW);

    Vertex::positionAttribPointer();
    Vertex::normalAttribPointer();
    Vertex::textureCoordAttribPointer();
    Vertex::tangentAttribPointer();
    Vertex::bitangentAttribPointer();

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
