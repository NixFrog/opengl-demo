#include <opengl-demo/graphics-engine/models/skybox.hpp>

using namespace GLDemo::GraphicsEngine;

Skybox::Skybox(std::string filepath)
{
	m_Faces.push_back(filepath + "/right.jpg");
	m_Faces.push_back(filepath + "/left.jpg");
	m_Faces.push_back(filepath + "/top.jpg");
	m_Faces.push_back(filepath + "/bottom.jpg");
	m_Faces.push_back(filepath + "/back.jpg");
	m_Faces.push_back(filepath + "/front.jpg");

	loadTexture();
}

GLuint Skybox::getTextureID() const
{
	return m_TextureID;
}

void Skybox::loadTexture()
{
	GLuint textureID;
	glGenTextures(1, &textureID);
	glActiveTexture(GL_TEXTURE0);

	int width,height;
	unsigned char* image;

	glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);
	if(!glIsTexture(textureID)){
		std::cerr << "Error in skybox generating texture" << std::endl;
		exit(EXIT_FAILURE);
	}

	for(unsigned int i=0; i<m_Faces.size(); i++){
		image = SOIL_load_image(m_Faces[i].c_str(), &width, &height, 0, SOIL_LOAD_RGB);
		if(image == NULL){
			std::cerr << "Error in skybox loading texture from file with SOIL: "<< m_Faces[i] << std::endl;
			exit(EXIT_FAILURE);
		}
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
		SOIL_free_image_data(image);
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	m_TextureID = textureID;
}
