#include <opengl-demo/graphics-engine/models/geometry.hpp>

using namespace GLDemo::GraphicsEngine;

const GLfloat Geometry::cubeModel[108] = {
	-1.0f,  1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	1.0f,  -1.0f, -1.0f,
	1.0f,  -1.0f, -1.0f,
	1.0f,   1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	1.0f,  -1.0f, -1.0f,
	1.0f,  -1.0f,  1.0f,
	1.0f,   1.0f,  1.0f,
	1.0f,   1.0f,  1.0f,
	1.0f,   1.0f, -1.0f,
	1.0f,  -1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	1.0f,   1.0f,  1.0f,
	1.0f,   1.0f,  1.0f,
	1.0f,  -1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	-1.0f,  1.0f, -1.0f,
	1.0f,   1.0f, -1.0f,
	1.0f,   1.0f,  1.0f,
	1.0f,   1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	1.0f,  -1.0f, -1.0f,
	1.0f,  -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	1.0f,  -1.0f,  1.0f
};

