#include <opengl-demo/graphics-engine/models/model.hpp>

using namespace GLDemo::GraphicsEngine;

Model::Model(std::string path)
{
	Assimp::Importer importer;
	auto scene = importer.ReadFile(
		path,
		aiProcess_Triangulate
		| aiProcess_GenNormals
		| aiProcess_FlipUVs
		| aiProcess_CalcTangentSpace
	);

	if(!scene || scene->mFlags == AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode){
		std::cerr << "Error with Assimp parsing " << path << ":\n"
				<< importer.GetErrorString() << std::endl;
		exit(EXIT_FAILURE);
	}

	auto directory = path.substr(0, path.find_last_of('/'));
	for(uint i = 0 ; i < scene->mNumMeshes; ++i) {
		auto mesh = scene->mMeshes[i];

		m_Meshes.push_back(Mesh( *mesh
				, *scene->mMaterials[mesh->mMaterialIndex]
				, directory
				));
	}
}

void Model::draw(ShaderManager& shaderManager, bool isTextured)
{
    for(auto mesh: m_Meshes){
        mesh.draw(shaderManager, isTextured);
    }
}

void Model::translate(const glm::vec3& vector)
{
    m_ModelMatrix = glm::translate(m_ModelMatrix, vector);
}

void Model::rotate(float angle, const glm::vec3& axis)
{
    m_ModelMatrix = glm::rotate(m_ModelMatrix, angle, axis);
}

void Model::scale(const glm::vec3& vector)
{
    m_ModelMatrix = glm::scale(m_ModelMatrix, vector);
}

void Model::reposition(const glm::vec3& vector){
    m_ModelMatrix[3] = glm::vec4(vector, 1.0f);
}

glm::mat3 Model::getNormalMatrix() const
{
    auto normalMatrix = glm::mat3(glm::inverseTranspose(m_ModelMatrix));
    return normalMatrix;
}
