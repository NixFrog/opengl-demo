#include <opengl-demo/graphics-engine/models/textures/material.hpp>

using namespace GLDemo::GraphicsEngine;

Material::Material(const aiMaterial& material, std::string filedir)
{
    loadTexture(material, filedir, aiTextureType_DIFFUSE, GL_TEXTURE0);
    m_NormalMapIsSet = loadTexture(material, filedir, aiTextureType_HEIGHT, GL_TEXTURE1);

    aiColor3D color;

    material.Get(AI_MATKEY_COLOR_AMBIENT, color);
    m_Ambient = glm::vec4(color.r, color.g, color.b, 1.0f);

    material.Get(AI_MATKEY_COLOR_DIFFUSE, color);
    m_Diffuse = glm::vec4(color.r, color.g, color.b, 1.0f);

    material.Get(AI_MATKEY_COLOR_SPECULAR, color);
    m_Specular = glm::vec4(color.r, color.g, color.b, 1.0f);

    material.Get(AI_MATKEY_SHININESS, m_Shininess);
}

void Material::update(ShaderManager shaderManager)
{
    shaderManager.setUniform("material.diffuseMap", (GLuint) 0);
    shaderManager.setUniform("material.normalMap", (GLuint) 1);

    shaderManager.setUniform("material.ambient", m_Ambient);
    shaderManager.setUniform("material.diffuse", m_Diffuse);
    shaderManager.setUniform("material.specular", m_Specular);
    shaderManager.setUniform("material.shininess", m_Shininess);
}

void Material::bindTextures()
{
    for(auto texture: m_Textures){
        texture.bind();
    }
}

void Material::unbindTextures()
{
    for(auto texture: m_Textures){
        texture.unbind();
    }
}

bool Material::loadTexture(const aiMaterial& material, std::string filedir
                          , const aiTextureType& type, const GLenum& unit)
{
    if (material.GetTextureCount(type) <= 0) {
        return false;
    }

    aiString textureFile;
    int opacity;

    material.GetTexture(type, 0, &textureFile);
    material.Get(AI_MATKEY_OPACITY, opacity);

    std::string texturePath = filedir + '/' + std::string(textureFile.C_Str());
    m_Textures.push_back(Texture(unit, texturePath, !opacity));

    return true;
}
