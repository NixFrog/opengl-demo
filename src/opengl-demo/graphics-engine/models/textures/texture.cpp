#include <opengl-demo/graphics-engine/models/textures/texture.hpp>

using namespace GLDemo::GraphicsEngine;

Texture::Texture(const GLenum& targetUnit, std::string filename, bool hasAlphaChannel)
: m_TargetUnit(targetUnit)
, m_Filename(filename)
, m_HasAlphaChannel(hasAlphaChannel)
, m_IsLoaded(false)
{
    load();
}

void Texture::load()
{
    int width, height;

    if(!m_IsLoaded){
        auto load = m_HasAlphaChannel ? SOIL_LOAD_RGBA : SOIL_LOAD_RGB;
        auto rgbMode = m_HasAlphaChannel ? GL_RGBA : GL_RGB;
        auto image = SOIL_load_image(m_Filename.c_str(), &width, &height, nullptr, load);

        if(image == nullptr){
            std::cerr << "Error loading texture from " << m_Filename << ":\n"
                      << SOIL_last_result() << std::endl;
            exit(EXIT_FAILURE);
        }

        glGenTextures(1, &m_Id);
        glBindTexture(GL_TEXTURE_2D, m_Id);

        if(!glIsTexture(m_Id)){
            std::cerr << "Error generating texture from " << m_Filename << std::endl;
            exit(EXIT_FAILURE);
        }

        glTexImage2D(GL_TEXTURE_2D, 0, rgbMode, width, height, 0, rgbMode, GL_UNSIGNED_BYTE, image);
        glGenerateMipmap(GL_TEXTURE_2D);

        SOIL_free_image_data(image);

        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

        glBindTexture(GL_TEXTURE_2D, 0);

        m_IsLoaded = true;
    }
    else{
        std::cerr << "Warning: loading texture twice:" << m_Filename << std::endl;
    }
}

void Texture::bind()
{
    glActiveTexture(m_TargetUnit);
    glBindTexture(GL_TEXTURE_2D, m_Id);
}

void Texture::unbind()
{
    glActiveTexture(m_TargetUnit);
    glBindTexture(GL_TEXTURE_2D, 0);
}
