#include <opengl-demo/game.hpp>

#define CALLBACK_INSTANCE GLDemo::GameEngine::InputCallbackAssociator::getInstance()

using namespace GLDemo;

Game::Game()
: m_Window(GLDemo::Util::createCurrentWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "OpenGL-Demo"))
{
	CALLBACK_INSTANCE.associateCallbacks(m_Window);
}

Game::~Game()
{
	m_CurrentScene.deactivate();
}

void Game::init()
{
	m_World.addSystem(*CALLBACK_INSTANCE.getInputHandler().getCameraSystem());
	m_World.addSystem(*CALLBACK_INSTANCE.getInputHandler().getPlayerSystem());
	m_World.addSystem(m_MovementSystem);
	m_World.addSystem(m_ModelRenderingSystem);
	m_World.addSystem(m_SkyboxRenderingSystem);
	m_World.addSystem(m_DirectionalShadowMappingSystem);
	m_World.addSystem(m_SSAORenderingSystem);

	m_Camera = m_World.createEntity();
	m_Camera.addComponent<GameEngine::CameraComponent>(
		glm::vec3(0.0f, 5.0f, -10.0f)
		, glm::vec3(0.0f, 0.0f, 0.0f)
		, glm::vec3(0.0f, 0.0f, 0.0f)
		, 800, 600, glm::radians(45.f), 0.1f, 500.f
		);
	m_Camera.activate();

	m_CurrentScene = GameEngine::SceneBuilder::buildDefaultScene(m_World);

	GraphicsEngine::Model playerModel("../resources/models/nanosuit/nanosuit.obj");
	playerModel.scale(glm::vec3(0.2f, 0.2f, 0.2f));
	m_Player = m_World.createEntity();
	m_Player.addComponent<GameEngine::PlayerComponent>();
	m_Player.addComponent<GameEngine::PositionComponent>(glm::vec3(0.0f, 0.0f, 0.0f));
	m_Player.addComponent<GameEngine::VelocityComponent>(glm::vec3(0.0f, 0.0f, 0.0f));
	m_Player.addComponent<GameEngine::ModelComponent>(playerModel);
	m_Player.activate();
}

void Game::run()
{
	auto startingTime = std::chrono::high_resolution_clock::now();
	while(!glfwWindowShouldClose(m_Window)){
		auto currentTime = std::chrono::high_resolution_clock::now();
		auto timeSinceLastUpdate = std::chrono::duration_cast<std::chrono::duration<float>>
				(currentTime - startingTime).count();

		startingTime = std::chrono::high_resolution_clock::now();

		processEvents();
		update(timeSinceLastUpdate);
		render();
	}

	glfwTerminate();
}

void Game::update(float timeSinceLastUpdate)
{
	m_World.refresh();
	CALLBACK_INSTANCE.getInputHandler().getCameraSystem()->update(timeSinceLastUpdate);
	CALLBACK_INSTANCE.getInputHandler().getPlayerSystem()->update(timeSinceLastUpdate
			, m_Camera.getComponent<GameEngine::CameraComponent>().getFrontVector()
			, m_Camera.getComponent<GameEngine::CameraComponent>().getRightVector()
			);

	m_MovementSystem.update(timeSinceLastUpdate);
	m_ModelRenderingSystem.update();
}

void Game::render()
{
	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT| GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	std::vector<GameEngine::PointLightComponent> pointLights;

	for(auto pointLight : m_CurrentScene.getPointLights()){
		pointLights.push_back(pointLight.getComponent<GameEngine::PointLightComponent>());
	}

	m_DirectionalShadowMappingSystem.render(
			m_CurrentScene.getDirectionalLight().getComponent<GameEngine::DirectionalLightComponent>());

	//m_SSAORenderingSystem.render(m_Camera.getComponent<GameEngine::CameraComponent>());
	m_ModelRenderingSystem.render(
			m_Camera.getComponent<GameEngine::CameraComponent>()
			, m_CurrentScene.getAmbientLight().getComponent<GameEngine::AmbientLightComponent>()
			, m_CurrentScene.getDirectionalLight().getComponent<GameEngine::DirectionalLightComponent>()
			, pointLights
			, m_DirectionalShadowMappingSystem.getShadowMap()
			, m_DirectionalShadowMappingSystem.getLightSpaceMatrix()
			);

	m_SkyboxRenderingSystem.render(m_Camera.getComponent<GameEngine::CameraComponent>());

	// MUST be at the end for gamma correction of default 2.2 factor
	glEnable(GL_FRAMEBUFFER_SRGB);
	glfwSwapBuffers(m_Window);
}

void Game::processEvents()
{
	glfwPollEvents();
}
