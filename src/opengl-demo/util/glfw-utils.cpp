#include <opengl-demo/util/glfw-utils.hpp>

void GLDemo::Util::initializeGLFW()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
}

GLFWwindow* GLDemo::Util::createCurrentWindow(size_t width, size_t height, const char* title)
{
    int realWidth, realHeight;

    initializeGLFW();

    auto window = glfwCreateWindow(width, height, title, nullptr, nullptr);

    if(!window){
        std::cerr<< "Failed to create GLFW window"<< std::endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);

    initializeGLEW();
    glfwGetFramebufferSize(window, &realWidth, &realHeight);
    glViewport(0, 0, realWidth, realHeight);

    return window;
}
