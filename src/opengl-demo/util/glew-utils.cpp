#include <opengl-demo/util/glew-utils.hpp>

void GLDemo::Util::initializeGLEW()
{
    glewExperimental = GL_TRUE;
    auto initStatus = glewInit();
    if(initStatus != GLEW_OK){
        std::cerr<< "Failed to initialize GLEW"<< std::endl;
    }
}
