#include <opengl-demo/game.hpp>

int main() {
    GLDemo::Game game;
	game.init();
    game.run();

    return 0;
}
