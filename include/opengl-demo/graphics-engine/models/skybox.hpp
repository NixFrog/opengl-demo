#pragma once

#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <SOIL/SOIL.h>

namespace GLDemo::GraphicsEngine
{
	class Skybox
	{
	public:
		Skybox(std::string filepath);
		GLuint getTextureID() const;

	private:
		void loadTexture();

		std::vector<std::string> m_Faces;
		GLuint m_TextureID;
	};
}
