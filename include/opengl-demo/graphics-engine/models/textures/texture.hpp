#pragma once

#include <iostream>

#include <GL/glew.h>
#include <SOIL/SOIL.h>

namespace GLDemo::GraphicsEngine
{
    class Texture
    {
    public:
        Texture(const GLenum& targetUnit, std::string filename, bool hasAlphaChannel = true);

        void load();
        void bind();
        void unbind();

    private:
        GLuint m_Id;
        GLenum m_TargetUnit;
        std::string m_Filename;
        bool m_HasAlphaChannel;
        bool m_IsLoaded;
    };
}
