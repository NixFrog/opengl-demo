#pragma once

#include <iostream>
#include <vector>

#include <GL/glew.h>
#include <assimp/material.h>
#include <glm/glm.hpp>

#include <opengl-demo/graphics-engine/shaders/shader-manager.hpp>
#include <opengl-demo/graphics-engine/models/textures/texture.hpp>

namespace GLDemo::GraphicsEngine
{
    class Material
    {
    public:
        Material(const aiMaterial& material, std::string filedir);

        void update(ShaderManager shaderManager);

        void bindTextures();
        void unbindTextures();

    private:
        bool loadTexture(const aiMaterial& material, std::string filedir
                         , const aiTextureType& type, const GLenum& unit);

        std::vector<Texture> m_Textures;
        bool m_NormalMapIsSet;
        glm::vec4 m_Ambient;
        glm::vec4 m_Diffuse;
        glm::vec4 m_Specular;
        float m_Shininess;
    };
}
