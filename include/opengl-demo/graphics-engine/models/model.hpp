#pragma once

#include <string>
#include <vector>
#include <memory>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

#include <opengl-demo/graphics-engine/shaders/shader-manager.hpp>
#include <opengl-demo/graphics-engine/models/components/mesh.hpp>

namespace GLDemo::GraphicsEngine
{
    class Model
    {
    public:
        Model(std::string path);
        void draw(ShaderManager& shaderManager, bool isTextured = true);

        void translate(const glm::vec3& vector);
        void rotate(float angle);
        void rotate(float angle, const glm::vec3& axis = glm::vec3(0.0f, 1.0f, 0.0f));
        void scale(const glm::vec3& vector);
        void reposition(const glm::vec3& vector);

        glm::mat4 getModelMatrix() const {return m_ModelMatrix;}
        glm::mat3 getNormalMatrix() const;

    private:
        void processNode(aiNode* node, const aiScene* scene);

        glm::mat4 m_ModelMatrix;
        std::vector<Mesh> m_Meshes;
    };
}
