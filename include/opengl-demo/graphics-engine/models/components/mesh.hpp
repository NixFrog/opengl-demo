#pragma once

#include <vector>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <assimp/mesh.h>

#include <opengl-demo/graphics-engine/models/components/vertex.hpp>
#include <opengl-demo/graphics-engine/models/textures/material.hpp>
#include <opengl-demo/graphics-engine/shaders/shader-manager.hpp>

namespace GLDemo::GraphicsEngine
{
    class Mesh
    {
    public:
        Mesh(const aiMesh& mesh, const aiMaterial& material, std::string materialDir);
        void draw(ShaderManager& shaderManager, bool isTextured);

    private:
        void generateBuffers();

        GLuint m_vao, m_vbo, m_ebo;
        std::vector<Vertex> m_Vertices;
        std::vector<GLuint> m_Indices;
        Material m_Material;
    };
}
