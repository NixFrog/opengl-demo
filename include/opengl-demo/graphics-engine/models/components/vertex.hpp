#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <assimp/mesh.h>

namespace GLDemo::GraphicsEngine
{
    struct Vertex
    {
    public:
        Vertex( const aiVector3D& position
              , const aiVector3D& normal
              , const aiVector3D& textureCoordinates
              , const aiVector3D& tangent
              , const aiVector3D& bitangent
              );

        static void positionAttribPointer();
        static void normalAttribPointer();
        static void textureCoordAttribPointer();
        static void tangentAttribPointer();
        static void bitangentAttribPointer();

    private:
        glm::vec3 m_Position;
        glm::vec3 m_Normal;
        glm::vec2 m_TextureCoordinates;
        glm::vec3 m_Tangent;
        glm::vec3 m_Bitangent;
    };
}
