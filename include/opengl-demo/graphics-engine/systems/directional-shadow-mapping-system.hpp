#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <anax/System.hpp>

#include <opengl-demo/graphics-engine/shaders/shader-utils.hpp>
#include <opengl-demo/game-engine/components/model-component.hpp>
#include <opengl-demo/game-engine/components/directional-light-component.hpp>
#include <opengl-demo/graphics-engine/shaders/shader-manager.hpp>

namespace GLDemo::GraphicsEngine
{
	class DirectionalShadowMappingSystem
		: public anax::System<anax::Requires<GameEngine::ModelComponent>>
	{
	public:
		DirectionalShadowMappingSystem();
		void render(const GameEngine::DirectionalLightComponent& directionalLight);
		GLuint getShadowMap() const;
		glm::mat4 getLightSpaceMatrix() const;

	private:
		GLuint newDepthMap();
		void generateNewMaps(size_t entitiesSize);
		void attachDepthMap(const GLuint& depthMap);

		ShaderManager m_ShaderManager;
		GLuint m_DepthMapFBO;
		GLuint m_ShadowMap;
		glm::mat4 m_LightSpaceMatrix;
		const GLuint SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
		const GLfloat NEAR_PLANE = 1.0f, FAR_PLANE = 100.0f;
		const glm::mat4 LIGHT_PROJECTION = glm::ortho(-80.0f, 80.0f, -80.0f, 80.0f, NEAR_PLANE, FAR_PLANE);
	};
}
