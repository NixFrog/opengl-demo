#pragma once

#include <vector>

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <opengl-demo/graphics-engine/systems/rendering-system.hpp>
#include <opengl-demo/game-engine/components/model-component.hpp>
#include <opengl-demo/game-engine/components/position-component.hpp>
#include <opengl-demo/game-engine/components/ambient-light-component.hpp>
#include <opengl-demo/game-engine/components/directional-light-component.hpp>
#include <opengl-demo/game-engine/components/point-light-component.hpp>

namespace GLDemo::GraphicsEngine
{
	class ModelRenderingSystem
		: public RenderingSystem<anax::Requires<GameEngine::ModelComponent, GameEngine::PositionComponent>>
	{
	public:
		ModelRenderingSystem();
		void update();
		void render(const GameEngine::CameraComponent& cameraComponent) override;
		void render(const GameEngine::CameraComponent& cameraComponent
				, const GameEngine::AmbientLightComponent& ambientLightComponent
				, const GameEngine::DirectionalLightComponent& directionalLight
				, std::vector<GameEngine::PointLightComponent> pointLights
				, GLuint shadowMap
				, glm::mat4 lightSpaceMatrix
				);

	private:
		ShaderManager m_ShaderManager;
		const size_t MAX_LIGHT_COUNT = 4;
	};
}
