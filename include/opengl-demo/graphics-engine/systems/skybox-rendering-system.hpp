#pragma once

#include <opengl-demo/graphics-engine/systems/rendering-system.hpp>
#include <opengl-demo/graphics-engine/components/skybox-component.hpp>
#include <opengl-demo/game-engine/components/model-component.hpp>

namespace GLDemo::GraphicsEngine
{
	class SkyboxRenderingSystem
		: public RenderingSystem<anax::Requires<GraphicsEngine::SkyboxComponent>>
	{
	public:
		SkyboxRenderingSystem();
		void render(const GameEngine::CameraComponent& cameraComponent) override;

	private:
		ShaderManager m_ShaderManager;
		Model m_Model;
	};
}
