#pragma once

#include <opengl-demo/graphics-engine/shaders/shader-utils.hpp>
#include <opengl-demo/graphics-engine/shaders/shader-manager.hpp>
#include <opengl-demo/game-engine/components/camera-component.hpp>

#include <anax/System.hpp>

namespace GLDemo::GraphicsEngine
{
	template<typename T>
	class RenderingSystem
		: public anax::System<T>
	{
	public:
		virtual ~RenderingSystem(){}
		virtual void render(const GameEngine::CameraComponent& cameraComponent) = 0;

	protected:
		ShaderManager m_ShaderManager;
	};
}
