#pragma once

#include <random>
#include <vector>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <anax/System.hpp>

#include <opengl-demo/graphics-engine/systems/rendering-system.hpp>
#include <opengl-demo/graphics-engine/shaders/shader-utils.hpp>
#include <opengl-demo/graphics-engine/shaders/g-buffer.hpp>
#include <opengl-demo/game-engine/components/model-component.hpp>
#include <opengl-demo/graphics-engine/shaders/shader-manager.hpp>

namespace GLDemo::GraphicsEngine
{
	class SSAORenderingSystem
		: public RenderingSystem<anax::Requires<GameEngine::ModelComponent>>
	{
	public:
		SSAORenderingSystem();
		void render(const GameEngine::CameraComponent& cameraComponent) override;

	private:
		void createFrameBuffer();
		void initiateKernel(std::default_random_engine& generator);

		// Accelerating interpolating function to ensure the kernel weights closer occlusions more
		GLfloat lerp(float a, float b, float f);

		// Generate a 4x4 array of random rotation vectors oriented around the tangent-space normal
		void generateNoise(std::default_random_engine& generator);
		void generateNoiseTexture();
		void generateQuadVAO();
		void renderQuad();

		std::vector<glm::vec3> m_ssaoKernel;
		std::vector<glm::vec3> m_ssaoNoise;
		GBuffer m_gBuffer;
		GLuint m_NoiseTexture, m_ssaoFBO, m_QuadVAO, m_ssaoColorBuffer, m_ssaoColorBufferBlurred;
		ShaderManager m_GeometryShaderManager, m_SSAOShaderManager;
	};
}
