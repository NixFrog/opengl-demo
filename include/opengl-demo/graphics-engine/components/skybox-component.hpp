#pragma once

#include <anax/Component.hpp>

#include <opengl-demo/graphics-engine/models/skybox.hpp>

namespace GLDemo::GraphicsEngine
{
	struct SkyboxComponent
		: anax::Component
	{
		Skybox skybox;

		SkyboxComponent(const Skybox& skybox)
			: skybox(skybox)
		{}
	};
}
