#pragma once

#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace GLDemo::GraphicsEngine
{
    class ShaderManager
    {
    public:
        ShaderManager();

        void addShader(const GLenum& type, std::string shaderFilename);
        void linkShaders();
        void use() const;

        GLuint getUniformLocation(std::string uniformName);
        void setUniform(std::string uniformName, const bool& value);
        void setUniform(std::string uniformName, const GLuint& value);
        void setUniform(std::string uniformName, const GLfloat& value);
        void setUniform(std::string uniformName, const glm::vec3& value);
        void setUniform(std::string uniformName, const glm::vec4& value);
        void setUniform(std::string uniformName, const glm::mat3& value);
        void setUniform(std::string uniformName, const glm::mat4& value);


    private:
        std::string readShader(std::string shaderFilename) const;
        void manageShaderCompilationError(const GLuint& shader, std::string shaderFilename) const;
        void manageLinkingError() const;

        GLuint m_ShaderProgram;
        std::vector<GLuint> m_Shaders;
        std::unordered_map<std::string, GLuint> m_UniformNameToIdMap;
    };
}
