#pragma once

#include <string>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace GLDemo::GraphicsEngine
{
	const std::string SHADER_DIR = "../src/opengl-demo/graphics-engine/shaders/";
	GLuint getNewGBuffer();
}
