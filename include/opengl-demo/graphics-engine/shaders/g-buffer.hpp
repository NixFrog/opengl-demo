#pragma once

#include <iostream>

#include <GL/glew.h>
#include <glm/glm.hpp>

namespace GLDemo::GraphicsEngine
{
	class GBuffer
	{
	public:
		GBuffer();
		GLuint getBuffer() const;
		GLuint getPositionDepthTexture() const;
		GLuint getNormalTexture() const;
		GLuint getAlbedoTexture() const;
		GLuint getDepthTexture() const;

	private:
		void generateGBuffer();
		void generatePositionDepthTexture();
		void generateNormalColorTexture();
		void generateAlbedoColorTexture();
		void generateDepthTexture();

		GLuint m_gBuffer, m_gPositionDepth, m_gNormal, m_gAlbedo;
	};
}
