#pragma once

#include <anax/System.hpp>

#include <opengl-demo/game-engine/components/camera-component.hpp>
#include <opengl-demo/game-engine/components/velocity-component.hpp>
#include <opengl-demo/util/direction-utils.hpp>

namespace GLDemo::GameEngine
{
	class CameraSystem
		: public anax::System<anax::Requires<CameraComponent>>
	{
    private:
        static constexpr float MIN_PITCH = -80.0f;
        static constexpr float MAX_PITCH = 60.0f;
        static constexpr float MIN_ZOOM  = 1.0f;
        static constexpr float MAX_ZOOM  = 300.0f;
        static constexpr float MOUSE_SENSITIVITY = 0.25f;

	public:
		void update(double timeElapsed);
        void processCursorMovement(
            const glm::dvec2& oldCursorPosition
            , const glm::dvec2& cursorPosition
        );
		void processKeyInput(Direction);
        void processScroll(double yoffset);

    private:
		void process(anax::Entity& entity, double timeElapsed);
        void translate(anax::Entity& entity, const glm::vec3& translation);
        void rotate(anax::Entity& entity, float angle, const glm::vec3& axis);
        void rotate(anax::Entity& entity, const glm::vec2& delta);
        void zoom(anax::Entity& entity, float value);

        std::vector< std::tuple< glm::dvec2, glm::dvec2>> m_CursorMovements;
        std::vector< double> m_yoffsets;
		std::vector<Direction> m_Directions;
		float m_MoveSpeed = 0.1f;
	};
}
