#pragma once

#include <anax/System.hpp>

#include <opengl-demo/game-engine/components/position-component.hpp>
#include <opengl-demo/game-engine/components/velocity-component.hpp>

namespace GLDemo::GameEngine
{
	class MovementSystem
		: public anax::System<anax::Requires<
			PositionComponent
			, VelocityComponent
		>>
	{
	public:
		void update(double timeElapsed);
	};
}
