#pragma once

#include <anax/System.hpp>

#include <opengl-demo/game-engine/components/player-component.hpp>
#include <opengl-demo/game-engine/components/velocity-component.hpp>
#include <opengl-demo/util/direction-utils.hpp>

namespace GLDemo::GameEngine
{
	class PlayerSystem
		: public anax::System<anax::Requires<PlayerComponent, VelocityComponent>>
	{
	public:
		void update(double timeElapsed, const glm::vec3& frontVector, const glm::vec3& rightVector);
		void processKeyInput(Direction direction);

	private:
		std::vector<Direction> m_Directions;
		float m_MoveSpeed = 0.1f;
	};
}
