#pragma once

#include <anax/World.hpp>

#include <opengl-demo/game-engine/scenes/scene.hpp>

#define XMINTREE -50
#define XMAXTREE 50
#define YMINTREE -50
#define YMAXTREE 50

namespace GLDemo::GameEngine
{
	class SceneBuilder
	{
	public:
		static Scene buildDefaultScene(anax::World& world);

	private:
		static void generateTrees(anax::World& world, size_t treeCount);
	};
}
