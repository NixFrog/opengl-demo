#pragma once

#include <anax/anax.hpp>

#include <vector>

namespace GLDemo::GameEngine
{
	class Scene
	{
	public:
		void deactivate();

		void setAmbientLight(const anax::Entity& ambientLight);
		void setDirectionalLight(const anax::Entity& directionalLight);
		void setPointLights(const std::vector<anax::Entity>& pointLights);
		void setUntrackedEntities(const std::vector<anax::Entity>& untrackedEntities);

		anax::Entity getAmbientLight() const;
		anax::Entity getDirectionalLight() const;
		std::vector<anax::Entity> getPointLights() const;

	private:
		anax::Entity m_AmbientLight;
		anax::Entity m_DirectionalLight;
		std::vector<anax::Entity> m_PointLights;
		std::vector<anax::Entity> m_UntrackedEntities;
	};
}
