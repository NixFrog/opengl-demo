#pragma once

#include <anax/anax.hpp>
#include <glm/glm.hpp>

namespace GLDemo::GameEngine
{
	struct CameraComponent
		: anax::Component
	{
		glm::vec3 position;
		glm::vec3 target;
		glm::vec3 targetOffset;
		float width;
		float height;
		float angle;
		float zNear;
		float zFar;

		CameraComponent(const glm::vec3& position
				, const glm::vec3& target
				, const glm::vec3 targetOffset
				, float width
				, float height
				, float angle
				, float zNear
				, float zFar
				)
			: position(position)
			, target(target)
			, targetOffset(targetOffset)
			, width(width)
			, height(height)
			, angle(angle)
			, zNear(zNear)
			, zFar(zFar)
		{}

		glm::mat4 getViewMatrix() const;
		glm::mat4 getProjectionMatrix() const;

		glm::vec3 getUpVector() const;
		glm::vec3 getRightVector() const;
		glm::vec3 getFrontVector() const;
	};
}
