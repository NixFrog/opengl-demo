#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <anax/anax.hpp>

namespace GLDemo::GameEngine
{
	struct VelocityComponent
		: anax::Component
	{
		glm::vec3 velocity;
		VelocityComponent(float x = 0.0f, float y = 0.0f, float z = 0.0f)
			: velocity(x, y, z)
		{}

		VelocityComponent(const glm::vec3& velocity)
			: velocity(velocity)
		{}
	};
}
