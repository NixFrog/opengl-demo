#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <anax/anax.hpp>

namespace GLDemo::GameEngine
{
	struct PositionComponent
		: anax::Component
	{
		glm::vec3 position;
		PositionComponent(float x = 0.0f, float y = 0.0f, float z = 0.0f)
			: position(x, y, z)
		{}

		PositionComponent(const glm::vec3& position)
			: position(position)
		{}
	};
}
