#pragma once

#include <glm/glm.hpp>
#include <anax/Component.hpp>

namespace GLDemo::GameEngine
{
	struct DirectionalLightComponent
		: anax::Component
	{
		glm::vec4 lightColor;
		glm::vec3 direction;

		DirectionalLightComponent(const glm::vec4& lightColor, const glm::vec3& direction)
			: lightColor(lightColor)
			, direction(direction)
		{}
	};
}
