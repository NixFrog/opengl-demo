#pragma once

#include <glm/glm.hpp>
#include <anax/Component.hpp>
#include <opengl-demo/graphics-engine/models/model.hpp>

namespace GLDemo::GameEngine
{
	struct PlayerComponent
		: anax::Component
	{
		PlayerComponent()
		{}
	};
}
