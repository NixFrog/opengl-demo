#pragma once

#include <glm/glm.hpp>
#include <anax/Component.hpp>

namespace GLDemo::GameEngine
{
	struct PointLightComponent
		: anax::Component
	{
		glm::vec4 lightColor;
		glm::vec3 position;
		float constant;
		float linear;
		float quadratic;

		PointLightComponent(const glm::vec4& lightColor, const glm::vec3& position
				, float constant, float linear, float quadratic
				)
			: lightColor(lightColor)
			, position(position)
			, constant(constant)
			, linear(linear)
			, quadratic(quadratic)
		{}
	};
}
