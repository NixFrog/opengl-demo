#pragma once

#include <anax/Component.hpp>
#include <opengl-demo/graphics-engine/models/model.hpp>

namespace GLDemo::GameEngine
{
	struct ModelComponent
		: anax::Component
	{
		GraphicsEngine::Model model;

		ModelComponent(const GraphicsEngine::Model& model)
			: model(model)
		{}
	};
}
