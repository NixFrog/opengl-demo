#pragma once

#include <glm/glm.hpp>
#include <anax/Component.hpp>

namespace GLDemo::GameEngine
{
	struct AmbientLightComponent
		: anax::Component
	{
		glm::vec4 lightColor;

		AmbientLightComponent(const glm::vec4& lightColor)
			: lightColor(lightColor)
		{}
	};
}
