#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <opengl-demo/game-engine/inputs/input-handler.hpp>

namespace GLDemo::GameEngine
{
    class InputCallbackAssociator
    {
    public:
        static InputCallbackAssociator& getInstance();
        static void associateCallbacks(GLFWwindow* window);
        static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
        static void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
        static void cursorPositionCallback(GLFWwindow* window, double xpos, double ypos);
        static void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);

        static InputHandler getInputHandler();

    private:
        InputCallbackAssociator();
        InputCallbackAssociator(InputCallbackAssociator const&);
        void operator=(InputCallbackAssociator const&);

        static InputHandler m_InputHandler;
    };
}
