#pragma once

#include <memory>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <opengl-demo/game-engine/systems/camera-system.hpp>
#include <opengl-demo/game-engine/systems/player-system.hpp>

namespace GLDemo::GameEngine
{
	class InputHandler
	{
	public:
		InputHandler() noexcept;

		std::shared_ptr<CameraSystem> getCameraSystem() const;
		std::shared_ptr<PlayerSystem> getPlayerSystem() const;

		void processKey(GLFWwindow* window, int key, int scancode, int action, int mods);
		void processMouseButton(GLFWwindow* window, int button, int action, int mods);
		void processCursorMovement(GLFWwindow* window, double xpos, double ypos);
		void processScroll(GLFWwindow* window, double xoffset, double yoffset);

	private:
		std::shared_ptr<CameraSystem> m_CameraSystem;
		std::shared_ptr<PlayerSystem> m_PlayerSystem;
		glm::dvec2 m_CursorPosition;
		bool m_CameraIsMoving;
	};
}
