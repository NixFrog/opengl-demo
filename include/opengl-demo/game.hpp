#pragma once

#include <chrono>

#include <GL/glew.h>
#include <anax/anax.hpp>
#include <vector>

#include <opengl-demo/util/glfw-utils.hpp>
#include <opengl-demo/util/window-utils.hpp>
#include <opengl-demo/game-engine/scenes/scene-builder.hpp>
#include <opengl-demo/game-engine/systems/movement-system.hpp>
#include <opengl-demo/game-engine/inputs/input-callback-associator.hpp>
#include <opengl-demo/graphics-engine/systems/model-rendering-system.hpp>
#include <opengl-demo/graphics-engine/systems/skybox-rendering-system.hpp>
#include <opengl-demo/graphics-engine/systems/ssao-rendering-system.hpp>
#include <opengl-demo/graphics-engine/systems/directional-shadow-mapping-system.hpp>

namespace GLDemo
{
	class Game
	{
	public:
		Game();
		~Game();
		void init();
		void run();

	private:
		void update(float timeSinceLastUpdate);
		void render();
		void processEvents();

	private:
		GLFWwindow* m_Window;

		anax::World m_World;
		anax::Entity m_Camera;
		anax::Entity m_Player;

		GameEngine::MovementSystem m_MovementSystem;
		GameEngine::Scene m_CurrentScene;

		GraphicsEngine::ModelRenderingSystem m_ModelRenderingSystem;
		GraphicsEngine::SkyboxRenderingSystem m_SkyboxRenderingSystem;
		GraphicsEngine::DirectionalShadowMappingSystem m_DirectionalShadowMappingSystem;
		GraphicsEngine::SSAORenderingSystem m_SSAORenderingSystem;
	};
}
