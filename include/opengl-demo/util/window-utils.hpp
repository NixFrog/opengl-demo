#pragma once

#include <opengl-demo/util/glfw-utils.hpp>
#include <opengl-demo/util/glew-utils.hpp>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
