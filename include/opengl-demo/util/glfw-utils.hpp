#pragma once

#include <memory>
#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <opengl-demo/util/glew-utils.hpp>

namespace GLDemo::Util
{
    void initializeGLFW();
    GLFWwindow* createCurrentWindow(size_t width, size_t height, const char* title);
}
