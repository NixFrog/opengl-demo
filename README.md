# OpenGL-demo
OpenGL demonstration for my own learning purposes. This is based on a school project that only asked for a shader-based lighting model, and a scene with several models.

Except the external libraries, all of this project's code has been written from scratch, without any boilerplate being given for the assignment.

##Content
This project is mostly based on an ECS (Entity-Component-System) pattern, based on libAnax. This has been done in order to easily create nice, interactive scenes in the future.

Currently, 3D-wise, this project implements the following:

* Loading complex models and applying their materials from .obj and .mtl files
* A fully functional camera
* Blinn-Phong lighting models
* Multiple directional, point and ambient light sources
* Cubemaps, used for skyboxes
* Gamma correction
* Directional shadow mapping, with corrections for artifacts such as "shadow acne" or oversampling, and the addition of PCF (percentage-closer filtering)
* SSAO (screen-space ambient occlusion)
* post-rendering as much of the pipeline as possible, in order to use ssao and shadow mapping at the same time, is underway



# Dependencies
glew, glfw3, assimp, pthreads, SOIL, anax

# Building the project
Once the libraries are installed, this project requires meson and ninja to compile the code.

This project was tested on Fedora 24 and 25 with X11 runing (it seems that glfw still uses deprecated functions making it unable to work with Wayland).

Once at the root of the code repertory, run the following commands to generate an executable in src/opengl-demo/:
```
mkdir -p build
cd build
meson ..
ninja
```